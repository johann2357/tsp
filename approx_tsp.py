
import matplotlib.pyplot as plt
import random
import time


City = complex


def Cities(n):
    "Make a set of n cities, each with random coordinates."
    return set(City(random.randrange(10, 890),
               random.randrange(10, 590)) for c in range(n))

random.seed()
cities8, cities10, = Cities(8), Cities(10)
cities100, cities1000 = Cities(100), Cities(1000)


def distance(A, B):
    "The distance between two points."
    return abs(A - B)


def first(collection):
    "Start iterating over collection, and return the first element."
    for x in collection:
        return x


def plot_tour(algorithm, cities):
    "Apply a TSP algorithm to cities, and plot the resulting tour."
    # Find the solution and time how long it takes
    t0 = time.clock()
    tour = algorithm(cities)
    t1 = time.clock()
    # Plot the tour as blue lines between blue circles,
    # and the starting city as a red square.
    plotline(list(tour) + [tour[0]])
    plotline([tour[0]], 'rs')
    plt.show()
    print(
        "{} city tour; total distance = {:.1f}; time = {:.3f} secs for {}".format(
        len(tour), total_distance(tour), t1-t0, algorithm.__name__)
    )


def total_distance(tour):
    "The total distance between each pair of consecutive cities in the tour."
    return sum(distance(tour[i], tour[i-1]) for i in range(len(tour)))


def plotline(points, style='bo-'):
    "Plot a list of points (complex numbers) in the 2-D plane."
    X, Y = XY(points)
    plt.plot(X, Y, style)


def XY(points):
    """Given a list of points, return two lists:
    X coordinates, and Y coordinates."""
    return [p.real for p in points], [p.imag for p in points]


def Cities(n):
    "Make a set of n cities, each with random coordinates."
    return set(City(random.randrange(10, 890),
               random.randrange(10, 590)) for c in range(n))


def plot_tree(tree):
    "Given a tree of {parent: [child...]}, plot the lines between points."
    for P in tree:
        for C in tree[P]:
            plotline([P, C], 'ko-')
    plt.show()
    print('{} node Minimum Spanning Tree of length: {:.1f}'.format(
        len(tree), sum(distance(P, C) for P in tree for C in tree[P])))


def MST(cities):
    """Given a set of cities, build a minimum spanning tree:
    a dict of the form {parent: [child...]},
    where parent and children are cities,
    and the root of the tree is first(cities)."""
    N = len(cities)
    edges = shortest_first([(A, B) for A in cities for B in cities if A is not B])
    tree = {first(cities): []}  # the first city is the root of the tree.
    while len(tree) < N:
        (A, B) = first((A, B) for (A, B) in edges if (A in tree) and (B not in tree))
        tree[A].append(B)
        tree[B] = []
    return tree


def shortest_first(edges):
    "Sort a list of edges so that shortest come first."
    edges.sort(key=lambda (A, B): distance(A, B))
    return edges

def MST_TSP(cities):
    "Create a minimum spanning tree and walk it in pre-order, omitting duplicates."
    return preorder_uniq(MST(cities), first(cities), [])

def preorder_uniq(tree, node, result):
    "Traverse tree in pre-order, starting at node, omitting repeated nodes."
    # Accumulate results in the 'result' parameter, which should start with an empty list
    if node not in result:
        result.append(node)
    for child in tree[node]:
        preorder_uniq(tree, child, result)
    return result

plot_tour(MST_TSP, cities10)
#plot_tour(greedy_TSP, cities100)

