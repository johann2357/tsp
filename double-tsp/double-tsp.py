def find_all_cycles(start, d):
    result = []
    n = len(d)

    def find_cycles(to, parents, actual_size=0, depth=0):
        if depth < n:
            for i, w in enumerate(d[parents[-1]]):
                if not w == -1:
                    if not i in parents:
                        find_cycles(
                            to,
                            parents + (i, ),
                            actual_size + w,
                            depth + 1
                        )
                    if i == to:
                        result.append((parents + (i, ), actual_size + w))
        return

    find_cycles(start, (start, ))
    return result


def is_complement(path1, path2, start, n):
    mask = [False for i in xrange(n)]
    mask[start] = True
    for i in path1[1:-1]:
        mask[i] = True
    for i in path2[1:-1]:
        if mask[i]:
            return False
        mask[i] = True
    for x in mask:
        if not x:
            return False
    return True


def compare_paths(start, paths, n):
    min_result = (None, 999999999)
    for i in xrange(len(paths)):
        for j in xrange(len(paths)):
            if i != j:
                if is_complement(paths[i][0], paths[j][0], start, n):
                    tmp = paths[i][1] + paths[j][1]
                    if min_result[1] > tmp:
                        min_result = ((paths[i][0], paths[j][0]), tmp)
    return min_result


if __name__ == '__main__':
    d = [
        [-1, 6, 2, 3],
        [4, -1, 1, 5],
        [1, 3, -1, 3],
        [2, 6, 7, -1]
    ]
    start = 1
    ans = compare_paths(start, find_all_cycles(start, d), len(d))
    if ans[0]:
        print "SALESMAN 1 PATH: ", ans[0][0]
        print "SALESMAN 2 PATH: ", ans[0][1]
        print "PATHS WEIGHT: ", ans[1]
    else:
        print "THERE ARE NO PATHS"
