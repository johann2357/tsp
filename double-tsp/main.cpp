#include <iostream>
#include <algorithm>
#include <vector>
#include <utility>
using namespace std;

#define N 4
#define START 1
#define INF 999999

int GRAPH[N][N] = {
  {-1, 6, 2, 3},
  {4, -1, 1, 5},
  {1, 3, -1, 3},
  {2, 6, 7, -1}
};
vector< pair< vector<int>, int > > CYCLES;

void find_cycles(vector<int> parents, int actual_size=0, int depth=0)
{
  if (depth < N)
  {
    for (int i = 0; i < N; ++i)
    {
      int* ws = GRAPH[parents.back()];
      if (ws[i] != -1)
      {
        if (count(parents.begin(), parents.end(), i) != 1)
        {
          parents.push_back(i);
          find_cycles(parents, actual_size + ws[i], depth + 1);
          parents.pop_back();
        }
        if (i == START)
        {
          parents.push_back(i);
          CYCLES.push_back(make_pair(parents, actual_size + ws[i]));
          parents.pop_back();
        }
      }
    }
  }
}

bool is_complement(vector<int> path1, vector<int> path2)
{
  bool mask[N];
  for (int i = 0; i < N; ++i)
    mask[i] = false;
  mask[START] = true;
  for (int i = 1; i < (path1.size() - 1); ++i)
    mask[path1[i]] = true;
  for (int i = 1; i < (path2.size() - 1); ++i)
  {
    if (mask[path2[i]])
      return false;
    mask[path2[i]] = true;
  }
  for (int i = 0; i < N; ++i)
  {
    if (! mask[i])
      return false;
  }
  return true;
}

pair< pair< vector<int>, vector<int> >, int > DTSP()
{
  vector<int> foo;
  foo.push_back(START);
  find_cycles(foo);
    pair< pair< vector<int>, vector<int> > , int>
  min_result = make_pair(make_pair(foo, foo), INF);
  int tmp;
  for (int i = 0; i < CYCLES.size(); ++i)
  {
    for (int j = 0; j < CYCLES.size(); ++j)
    {
      if (i != j)
      {
        if (is_complement(CYCLES[i].first, CYCLES[j].first))
        {
          tmp = CYCLES[i].second + CYCLES[j].second;
          if (min_result.second > tmp)
            min_result = make_pair(
              make_pair(CYCLES[i].first, CYCLES[j].first),
              tmp
            );
        }
      }
    }
  }
  return min_result;
}

int main()
{
  pair< pair< vector<int>, vector<int> > , int> ans;
  ans = DTSP();
  if (ans.second != INF)
  {
    cout << "SALESMAN 1 PATH:" << endl;
    for (int i = 0; i < ans.first.first.size(); ++i)
      cout << ans.first.first[i] << endl;
    cout << "SALESMAN 2 PATH:" << endl;
    for (int i = 0; i < ans.first.second.size(); ++i)
      cout << ans.first.second[i] << endl;
    cout << "PATHS WEIGHT: " << ans.second << endl;
  }
  else
    cout << "THERE ARE NO PATHS" << endl;
  return 0;
}
